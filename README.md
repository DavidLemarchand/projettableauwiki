Goal of the project
===

    The objective of this project is to create a program allowing from a list of urls pointing on Wikipedia pages to extract the maximum of tables contained in these pages and export them in csv format in the output folder of the project.

License
===

    GNU V3

Maven
===
    Add dependencies in the pom.xml

```java
    <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>fr.univrennes1.istic.wikipediamatrix</groupId>
  <artifactId>wikimatrix</artifactId>
  <packaging>jar</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>wikimatrix</name>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
    </dependency>
    
    <!-- https://mvnrepository.com/artifact/org.jsoup/jsoup -->
		<dependency>
		    <groupId>org.jsoup</groupId>
		    <artifactId>jsoup</artifactId>
		    <version>1.12.1</version>
		</dependency>
		
	
	<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-csv</artifactId>
    <version>1.7</version>
</dependency>

  </dependencies>
</project>


```

Launch the extractor
===

  Go to class BenchTest. Then execute it, the results will be in the output file.

UML Diagram
===

![](class-diagram.png)

