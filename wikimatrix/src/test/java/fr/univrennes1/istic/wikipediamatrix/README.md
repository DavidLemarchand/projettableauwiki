Statistiques
===

Nous avons calculés quelques statistiques relatives aux tableaux extraits :

* Moyenne du nombre de lignes par tableau : 21
* Ecart-type du nombre de ligne par tableau : 32.5
* Moyenne du nombre de colonnes par tableau : 5
* Minimum de lignes dans un tableau : 1
* Maximum de lignes dans un tableau : 452

Note : le nombre de lignes ne prend pas en compte l'en-tête.