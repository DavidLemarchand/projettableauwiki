package fr.univrennes1.istic.wikipediamatrix;

import static org.junit.Assert.*;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Element;
import org.junit.Test;

import beans.Ligne;
import beans.Tableau;
import service.ExtracteurService;
import service.TableauService;

public class BenchTest {
	
	/*
	*  the challenge is to extract as many relevant tables as possible
	*  and save them into CSV files  
	*  from the 300+ Wikipedia URLs given
	*  see below for more details
	**/
	
	TableauService tableauService = new TableauService();
	
	
	
	@Test
	public void testBenchExtractors() throws Exception {
		
		String BASE_WIKIPEDIA_URL = "https://en.wikipedia.org/wiki/";
		// directory where CSV files are exported (HTML extractor) 
		String outputDirHtml = "output" + File.separator + "html" + File.separator;
		assertTrue(new File(outputDirHtml).isDirectory());
		// directory where CSV files are exported (Wikitext extractor) 
		String outputDirWikitext = "output" + File.separator + "wikitext" + File.separator;
		assertTrue(new File(outputDirWikitext).isDirectory());
		
		File file = new File("inputdata" + File.separator + "wikiurls.txt");
		BufferedReader br = new BufferedReader(new FileReader(file));
	    String url;
	    int nurl = 0;
	    int nurl_inexistante=0;
	    List<Tableau> listAllTab=new ArrayList<Tableau>(); ;
	    while ((url = br.readLine()) != null) {
	       String wurl = BASE_WIKIPEDIA_URL + url; 
	       
		       //System.out.println("Wikipedia url: " + wurl);
		       int nTab = 1;
		       // TODO: do something with the Wikipedia URL 
		       // (ie extract relevant tables for correct URL, with the two extractors)
		    
		    	List<Tableau> listTab=tableauService.setTableaux(wurl);
		    	
		       
		       // for exporting to CSV files, we will use mkCSVFileName 
		       // example: for https://en.wikipedia.org/wiki/Comparison_of_operating_system_kernels
		       // the *first* extracted table will be exported to a CSV file called 
		       // "Comparison_of_operating_system_kernels-1.csv"
		    	if (!listTab.isEmpty()) {
			       for (Tableau tab : listTab) {
				    	
			    	   listAllTab.add(tab);
				       String csvFileName = mkCSVFileName(url, nTab);
				       
				       tableauService.saveTableauCsv(tab,csvFileName);
				       nTab++;
				       
				       
				       Reader reader = Files.newBufferedReader(Paths.get(outputDirHtml+csvFileName));
				   		
			   			
			    	   assertNotNull(reader);
						try {
							CSVParser parser = CSVParser.parse(reader, CSVFormat.RFC4180);
						
							//Tableau extrait est non vide
							assertNotNull(parser);
						} catch (Exception e) {
							System.out.println(e);
						}	
			       }
			       // the *second* (if any) will be exported to a CSV file called
			       // "Comparison_of_operating_system_kernels-2.csv"
		
			       
			       // TODO: the HTML extractor should save CSV files into output/HTML
			       // see outputDirHtml 
			       
			       // TODO: the Wikitext extractor should save CSV files into output/wikitext
			       // see outputDirWikitext      
			       
			       nurl++;	 
			       
		    	}else {
		    		nurl_inexistante++;
		    	}
	    }
	    
	    br.close();	    
	    assertEquals(nurl+nurl_inexistante, 336);
	    
	    afficherStats (listAllTab);


	}
	
	
	@Test
	public void testNbColonnesSupUn() throws Exception {
		
		File folder = new File("output" + File.separator + "html");
		File[] listOfFiles = folder.listFiles();
		int nbTableauPb=0;
	
		for (int i = 0; i < listOfFiles.length; i++) {
			
		
			try {
		            Reader reader = Files.newBufferedReader(Paths.get("output" + File.separator + "html"+ File.separator +listOfFiles[i].getName()));
		            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
		         
		            for (CSVRecord csvRecord : csvParser) {
		
		                
		                assertTrue(csvRecord.size()>1);
		            }
			}catch (AssertionError e) {
			
				nbTableauPb++;
		        	
		    }
			
		}
		
		System.out.println("nombre tableau avec 1 seule colonne :" + nbTableauPb);
	}
	
	
	
	@Test
	public void testNbColonnesEgal() throws Exception {
		
		File folder = new File("output" + File.separator + "html");
		File[] listOfFiles = folder.listFiles();
		int nbTableauPb=0;
	
		for (int i = 0; i < listOfFiles.length; i++) {
			
		
			try {
		            Reader reader = Files.newBufferedReader(Paths.get("output" + File.separator + "html"+ File.separator +listOfFiles[i].getName()));
		            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
		            
		            int nbColLignePrecedente=-1;
		         
		            for (CSVRecord csvRecord : csvParser) {		            	
		
		                assertTrue((nbColLignePrecedente==csvRecord.size())||(nbColLignePrecedente==-1));
		                
		                
		                nbColLignePrecedente=csvRecord.size();
		            }
			}catch (AssertionError e) {

				nbTableauPb++;
		        	
		    }
			
		}
		
		System.out.println("nombre tableau avec nombre de colonnes différent :" + nbTableauPb);
	}
	
	

	private String mkCSVFileName(String url, int n) {
		return url.replace('/', '\\').trim() + "-" + n + ".csv";
	}
	
	
	
	private double calculMoyenneLignes (List<Tableau> tableau) {
		/*total sera le nombre total de lignes de tous les tableaux*/
		int total=0;
		int nbTableau=0;
		/*pour chaque tableau de la liste*/
		for(Tableau tab : tableau) {
			if (!tab.getBody().isEmpty()) {
				nbTableau++;
				List<Ligne> corps = new ArrayList<Ligne>();
				/* on récèpère le body */
				corps=tab.getBody();
				/*taille du body (nb de lignes)*/
				int taille=corps.size();
				total+=taille;
			}
		}
		return total/nbTableau;
	}

	/*moyenne du nombre de colonnes par tableau*/
	private double calculMoyenneCol (List<Tableau> tableau) {
		/*total sera le nb total de colonnes de tous les tableaux*/
		int total=0;
		int nbTableau=0;
		for(Tableau tab : tableau) {
			if (!tab.getBody().isEmpty()) {
				nbTableau++;
				/* on récèpère la 1ère ligne */
				Ligne ligne1=tab.getBody().get(0);
				
				/* on récupère le nombre de cellules de la ligne et donc le nombre de colonnes*/
				int tailleLigne=ligne1.getListCellules().size();
				total+=tailleLigne;
			}
		}
		return total/nbTableau;
	}


	private double ecartType (List<Tableau> tableau) {
		double sommeErreurCarre=0;
		double moyenne=calculMoyenneLignes(tableau);
		int nbTableau=0;
		for(Tableau tab : tableau) {
			if (!tab.getBody().isEmpty()) {
				nbTableau++;
				List<Ligne> corps = new ArrayList<Ligne>();
				/* on récèpère le body */
				corps=tab.getBody();
				/*taille du body (nb de lignes)*/
				int taille=corps.size();
				sommeErreurCarre+=Math.pow((taille-moyenne), 2);
			}
		}
		return Math.sqrt(sommeErreurCarre/nbTableau);
					
		}


	private int minimumLignes (List<Tableau> tableau) {
		int min=tableau.get(0).getBody().size();
		for(Tableau tab : tableau) {
			if (!tab.getBody().isEmpty()) {
				List<Ligne> corps = new ArrayList<Ligne>();
				/* on récèpère le body */
				corps=tab.getBody();
				/*taille du body (nb de lignes)*/
				int taille=corps.size();
				/* on compare pour choisir la taille la plus petite */
				if(taille<min) {
					min=taille;
				}
			}
		}
		return min;
	}

	private int maximumLignes (List<Tableau> tableau) {
		int max=tableau.get(0).getBody().size();
		for(Tableau tab : tableau) {
			if (!tab.getBody().isEmpty()) {
				List<Ligne> corps = new ArrayList<Ligne>();
				/* on récèpère le body */
				corps=tab.getBody();
				/*taille du body (nb de lignes)*/
				int taille=corps.size();
				/* on compare pour choisir la taille la plus petite */
				if(taille>max) {
					max=taille;
				}
			}
		}
		return max;
	}


	private void afficherStats (List<Tableau> tableau) {
		System.out.println("MoyenneLignes :"+calculMoyenneLignes(tableau));
		System.out.println("MoyenneCol :"+calculMoyenneCol(tableau));
		System.out.println("Ecart-type :"+ecartType(tableau));

		System.out.println("Minimum :"+minimumLignes(tableau));
		System.out.println("Maximum :"+maximumLignes(tableau));
	}
	
	
	

}
