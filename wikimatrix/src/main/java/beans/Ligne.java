package beans;

import java.util.ArrayList;
import java.util.List;


public class Ligne {

	
	protected List<String> listCellules ;
	
	public Ligne () {
		this.listCellules=new ArrayList<String>();
	}
	
	public void setLigne(String elementLigne) {
		listCellules.add(elementLigne);
		
	}

	public List<String> getListCellules() {
		return listCellules;
	}

	
	

}
