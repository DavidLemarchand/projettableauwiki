package beans;

import java.util.ArrayList;
import java.util.List;

public class Tableau {
	
	private Header header;
	private List<Ligne> body;
	
	public Tableau() {
		this.header=new Header();
		this.body=new ArrayList<Ligne>();
	}
	
	public void setHeader(Header header) {
		this.header=header;
	}
	
	public Header getHeader() {
		return(header);
	}
	
	public List<Ligne> getBody() {
		return(body);
	}
	
	public void addLigne(Ligne ligne) {
		body.add(ligne);
	}
	

}
