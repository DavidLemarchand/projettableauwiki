package fr.univrennes1.istic.wikipediamatrix;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import beans.Tableau;
import dao.DaoCsv;
import dao.DaoHtml;
import service.ExtracteurService;
import service.TableauService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;


public class App 
{
    public static void main( String[] args ) throws Exception{
    	Document doc = Jsoup.connect("https://en.wikipedia.org/wiki/Comparison_of_browser_engines_(graphics_support)").get();
    	DaoHtml daoHtml = new DaoHtml();
    	DaoCsv daoCsv = new DaoCsv();
    	TableauService tableauService = new TableauService();
    	ExtracteurService service = new ExtracteurService();
    	
    	Tableau tab = new Tableau();
    	
    	tab=tableauService.setTableauCanon();
    	tableauService.saveTableauCsv(tab,"test");
    	
    	
    }
    	
	
		   
}
