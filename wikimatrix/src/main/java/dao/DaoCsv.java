package dao;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import beans.Ligne;
import beans.Tableau;

public class DaoCsv {
	
	public void writeCsv (Tableau tableau, String url) throws IOException {
		String outputDirHtml = "output" + File.separator + "html" + File.separator;
        try (
        		
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputDirHtml+url));

            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
                    
        ) {
            csvPrinter.printRecord(tableau.getHeader().getListCellules());
            for (Ligne ligne : tableau.getBody()) {
            	csvPrinter.printRecord(ligne.getListCellules());
            }
            	
            
            csvPrinter.flush();            
        }
	}
        
	
	
	
	

}
