package service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import beans.Ligne;
import beans.Tableau;
import dao.DaoCsv;
import dao.DaoHtml;

public class TableauService {
	
	private ExtracteurService extracteur = new ExtracteurService();
	private DaoCsv daoCsv = new DaoCsv();
	private DaoHtml daoHtml = new DaoHtml();
	
	public List<Tableau> setTableaux(String url) throws Exception {
		
		List<Tableau> listeTableaux = new ArrayList<Tableau>();
		try {
		Document pageHtml=daoHtml.getPage(url);
		
		
		Elements tableauxHtml=extracteur.extraireTableaux(pageHtml);
		
		for (Element elemTableau : tableauxHtml) {
			Tableau tableau = new Tableau();
			
			try {
				tableau.setHeader(extracteur.extraireHeader(elemTableau));
				
				
				if(tableau.getHeader().getListCellules().size()>1) {
					
					for(Ligne ligne: this.extracteur.extraireBody(elemTableau)) {
						tableau.addLigne(ligne);
					}
					listeTableaux.add(tableau);
				}else if(tableau.getHeader().getListCellules().size()==1){//il faut transposer le tableau
					
					
					tableau.setHeader(extracteur.extraireHeaderTranspose(elemTableau));
					
					for(Ligne ligne: this.extracteur.extraireBodyTranspose(elemTableau)) {
						tableau.addLigne(ligne);
					}
					listeTableaux.add(tableau);
				}
			}catch (IndexOutOfBoundsException e) {
				System.out.println("impossible de créer le tableau");
			}
				
		}
			return(listeTableaux);
			
		}
		catch (HttpStatusException e) {
			System.out.println("url inexistante");
			return(listeTableaux);
		}
	}
	
	public Tableau setTableauCanon() throws Exception {
		Element tableauHtml = extracteur.extraireCanon();
		Tableau tableau = new Tableau();
		
		tableau.setHeader(this.extracteur.extraireHeader(tableauHtml));
		
		for(Ligne ligne: this.extracteur.extraireBody(tableauHtml)) {
			tableau.addLigne(ligne);
		}
		return(tableau);
	}
	
	public void saveTableauCsv(Tableau tableau, String url) throws IOException {
		
		daoCsv.writeCsv(tableau, url);
		
	}
	

}
