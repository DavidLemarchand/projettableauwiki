package service;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import beans.Header;
import beans.Ligne;
import dao.DaoHtml;

public class ExtracteurService {
	
	DaoHtml daoHtml = new DaoHtml();
	
	public Element extraireCanon() throws Exception {
		Document doc = daoHtml.getTestPage();
		Element table = doc.select("table").get(0);
		return(table);
		
	}	
	
	
	public Elements extraireTableaux (Document doc) {
		//Elements table = doc.select("table");
		Elements table = doc.getElementsByClass("wikitable");
		return(table);
		
	}
	
	public Header extraireHeader(Element table) {
		Element rowHead = table.select("tr").get(0);
		Elements cellsHead= rowHead.select("th");

		Header header=new Header();
		
		
		for (Element elem : cellsHead) {
			
			header.setLigne(elem.text());	
	    }	
		
		return(header);
			
	}
		
	public Header extraireHeaderTranspose(Element table) {
			
		Elements cellsHead= table.select("th");
		Header header=new Header();
			
			
		for (Element elem : cellsHead) {
				
			header.setLigne(elem.text());	
	    }	
			
		return(header);
				
	}
	
	public List<Ligne> extraireBody(Element table) {
		List<Ligne> listLignes = new ArrayList<Ligne>();
		Elements rows = table.select("tr");	
		
		for (int i = 1; i < rows.size(); i++) { //first row is the col names so skip it.
		    Element row = rows.get(i);
		    Elements cells = row.select("td");
		    Ligne ligne=new Ligne();
		    for (Element elem : cells) {
		    	ligne.setLigne(elem.text());
		    }
		    listLignes.add(ligne);
		}
		
		return(listLignes);
			
	}
	
	public List<Ligne> extraireBodyTranspose(Element table) {
		List<Ligne> listLignes = new ArrayList<Ligne>();
		Elements rows = table.select("tr");	
		
		Element rowTest = rows.get(0);
		Elements cellsTest = rowTest.select("td");
		
		
			
		for (int j=0; j < cellsTest.size(); j++) {
			Ligne ligne=new Ligne();
			for (int i = 0; i < rows.size(); i++) { 							

				Element row = rows.get(i);
				Element cell = row.select("td").get(j);
				    
				ligne.setLigne(cell.text());
				    
				    
			}
			listLignes.add(ligne);
		}
			
		
		
		return(listLignes);
			
	}

}
